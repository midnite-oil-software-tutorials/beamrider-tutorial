using System.Collections;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] AudioClip _explosionSound;

    Animator _animator;

    bool AnimationFinished
    {
        get
        {
            if (!_animator) return true;
            var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            return stateInfo.normalizedTime >= 1f;
        }
    }

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    IEnumerator Start()
    {
        if (_explosionSound)
        {
            SoundManager.Instance.PlayAudioClip(_explosionSound);
        }
        while (!AnimationFinished) yield return null;
        Destroy(gameObject);
    }
}
