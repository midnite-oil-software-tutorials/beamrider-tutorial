using UnityEngine;

public class BeamMover : MonoBehaviour
{
    [SerializeField] float _initialMoveSpeed = 0.05f;
    [SerializeField] float _scaleAndSpeedIncrement = 0.01f;
    [SerializeField] float _bottomPosition = -5f;
    
    Transform _transform;
    float _moveSpeed;

    bool ReachedBottom => _transform.position.y < _bottomPosition;

    void Awake()
    {
        _transform = transform;
        _moveSpeed = _initialMoveSpeed;
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlaying) return;
        MoveBeam();
        EnlargeBeam();
        if (ReachedBottom)
        {
            Destroy(gameObject);
        }
    }

    void MoveBeam()
    {
        _transform.position += Vector3.down * (_moveSpeed * Time.deltaTime);
        _moveSpeed += _scaleAndSpeedIncrement;
    }

    void EnlargeBeam()
    {
        var localScale = _transform.localScale;
        localScale.y += (_scaleAndSpeedIncrement * Time.deltaTime);
        _transform.localScale = localScale;
    }
}
