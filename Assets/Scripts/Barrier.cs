using UnityEngine;

public class Barrier : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent<IHitBarrier>(out var hit))
        {
            hit.HitBarrier();
        }
    }
}
