using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class UserInput : SingletonMonoBehavior<UserInput>
{
    public event Action<InputValue> OnMoveReceived = delegate(InputValue value) {  };
    public event Action OnFirePressed = delegate {  };
    public event Action OnTorpedoPressed = delegate {  };
    public event Action OnQuitPressed = delegate {  };
    
    public Vector2 MoveInput { get; private set; }

    void OnMove(InputValue value)
    {
        MoveInput = value.Get<Vector2>();
        OnMoveReceived(value);
    }

    void OnFire() => OnFirePressed();
    void OnTorpedo() => OnTorpedoPressed();

    void OnQuit()
    {
        Debug.Log($"Quit pressed");
        OnQuitPressed();        
    }
}
