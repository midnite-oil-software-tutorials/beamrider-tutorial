using UnityEngine;

public class SingletonMonoBehavior<T> : MonoBehaviour where T : MonoBehaviour
{
    static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance != null) return _instance;

            _instance = FindObjectOfType<T>();
            if (_instance != null) return _instance;

            var singletonObject = new GameObject(typeof(T).Name);
            _instance = singletonObject.AddComponent<T>();

            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (Instance == null || Instance == this) return;
        Destroy(gameObject);
    }
}