using System;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : SingletonMonoBehavior<SoundManager>
{
    AudioSource _audioSource;

    public void PlayAudioClip(AudioClip clip, float volume = 1f)
    {
        _audioSource.PlayOneShot(clip, volume);
    }

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }
}
