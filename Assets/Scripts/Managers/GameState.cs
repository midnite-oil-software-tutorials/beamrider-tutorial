public enum GameState
{
    WaitingToStart,
    Playing,
    GameOver
}