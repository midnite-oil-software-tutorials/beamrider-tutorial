using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager : SingletonMonoBehavior<GameManager>
{
    public event Action<GameState> GameStateChanged = delegate(GameState state) {  };
    public event Action<int> ScoreChanged = delegate(int i) {  };
    public event Action<int> TorpedoesUpdated = delegate(int i) {  };
    public event Action<int> PlayerLivesChanged = delegate(int i) {  };  
    public event Action<int> EnemiesUpdated = delegate(int i) {  };
    public event Action<int> SectorUpdated = delegate(int i) {  };
    
    public GameState GameState { get; private set; }
    public bool IsPlaying => GameState == GameState.Playing;
    public int Torpedoes { get; set; }
    public int Sector { get; private set; }
    public int Score { get; private set; }
    public int PlayerLives { get; private set; }
    public int EnemiesUntilBoss { get; private set; }

    public void FireTorpedo()
    {
        UpdateTorpedoes(Math.Max(Torpedoes - 1, 0));
    }

    public void NextSector()
    {
        ++Sector;
        EnemiesUntilBoss = 10 + (2 * Sector);
        Torpedoes = 3;
        UpdateEnemies(EnemiesUntilBoss);
        UpdateSector(Sector);
        UpdateTorpedoes(Torpedoes);
    }

    public void AddPoints(int points)
    {
        Score += points;
        ScoreChanged(Score);
    }

    public void StartGame()
    {
        ResetScore();
        UpdateTorpedoes(3);
        UpdatePlayerLives(3);
        UpdateEnemies(12);
        UpdateSector(1);
        SpawnPlayerShip();
        SetGameState(GameState.Playing);
    }

    [SerializeField] PlayerShip _playerShipPrefab;
    [SerializeField] GameObject _startScreen, _gameOverScreen;
    WaitForSeconds _spawnPlayerDelay;

    void Start()
    {
        _spawnPlayerDelay = new WaitForSeconds(3f);
    }

    void OnEnable()
    {
        SetGameState(GameState.WaitingToStart);
    }

    void ResetScore()
    {
        Score = 0;
        ScoreChanged(Score);
    }

    void UpdateTorpedoes(int torpedoes)
    {
        Torpedoes = torpedoes;
        TorpedoesUpdated(Torpedoes);
    }

    void UpdatePlayerLives(int playerLives)
    {
        PlayerLives = playerLives;
        PlayerLivesChanged(PlayerLives);
    }

    void UpdateEnemies(int enemies)
    {
        EnemiesUntilBoss = enemies;
        EnemiesUpdated(EnemiesUntilBoss);
    }

    void UpdateSector(int sector)
    {
        Sector = sector;
        SectorUpdated(Sector);
    }

    void SpawnPlayerShip()
    {
        Instantiate(_playerShipPrefab);
    }

    void SetGameState(GameState gameState)
    {
        GameState = gameState;
        GameStateChanged(GameState);
    }

    public void EntityDied(Killable entity)
    {
        HandleEnemyDeath(entity);
        HandleBossDeath(entity);
        HandlePlayerDeath(entity);
    }

    void HandleEnemyDeath(Killable entity)
    {
        if (!entity.gameObject.TryGetComponent<EnemyShip>(out var enemy)) return;
        UpdateEnemies(Math.Max(EnemiesUntilBoss - 1, 0));
    }

    void HandleBossDeath(Killable entity)
    {
        if (!entity.gameObject.TryGetComponent<Boss>(out var boss)) return;
        NextSector();
    }

    void HandlePlayerDeath(Killable entity)
    {
        if (!entity.gameObject.TryGetComponent<PlayerShip>(out var player)) return;
        UpdatePlayerLives(PlayerLives - 1);
        if (PlayerLives < 1)
        {
            GameOver();
            return;
        }

        StartCoroutine(SpawnPlayerShipAfterDelay());
    }

    IEnumerator SpawnPlayerShipAfterDelay()
    {
        yield return _spawnPlayerDelay;
        SpawnPlayerShip();
    }

    void GameOver()
    {
        SetGameState(GameState.GameOver);
        _gameOverScreen.SetActive(true);
    }
}