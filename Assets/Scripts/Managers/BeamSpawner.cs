using System;
using UnityEngine;

public class BeamSpawner : MonoBehaviour
{
    [SerializeField] BeamMover _beamPrefab;
    [SerializeField] float _spawnInterval = 0.25f;

    float _nextSpawnTime;

    bool ShouldSpawnBeam => GameManager.Instance.IsPlaying && (Time.time >= _nextSpawnTime);

    void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
    }

    void Update()
    {
        if (ShouldSpawnBeam)
        {
            SpawnBeam();
        }
    }

    void SpawnBeam()
    {
        _nextSpawnTime = Time.time + _spawnInterval;
        Instantiate(_beamPrefab, transform);
    }

    void OnGameStateChanged(GameState gameState)
    {
        if (gameState == GameState.Playing)
        {
            StartSpawning();
        }
    }

    void StartSpawning()
    {
        _nextSpawnTime = Time.time + _spawnInterval;
    }
}
