using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] EnemyShip _enemyShipPrefab;
    [SerializeField] Boss _bossPrefab;
    [SerializeField] int _maxActiveEnemiesBase = 8;
    [SerializeField] float _addEnemyDelayMin = 1f, _addEnemyDelayMax = 3f;
    
    Transform _transform;
    float _spawnTime;
    bool _bossSpawned;

    int MaxEnemies => _maxActiveEnemiesBase + (GameManager.Instance.Sector * 2);

    bool ShouldSpawnEnemy
    {
        get
        {
            if (!GameManager.Instance.IsPlaying) return false;
            if (_transform.childCount >= MaxEnemies) return false;
            return Time.time >= _spawnTime;
        }
    }

    void Awake()
    {
        _transform = transform;
    }

    void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
    }

    void OnDisable()
    {
        GameManager.Instance.GameStateChanged -= OnGameStateChanged;
    }

    void Update()
    {
        if (ShouldSpawnEnemy)
        {
            SpawnEnemy();
        }

    }

    void OnGameStateChanged(GameState gameState)
    {
        if (gameState == GameState.Playing)
        {
            StartSpawning();
        }
    }

    void StartSpawning()
    {
        GameManager.Instance.EnemiesUpdated += OnEnemiesUpdated;
        GameManager.Instance.SectorUpdated += OnSectorUpdated;
        SpawnInitialEnemies();
    }

    void SpawnInitialEnemies()
    {
        for (var i = 0; i < 4 + (2 * GameManager.Instance.Sector); ++i)
        {
            SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        SetSpawnTime();
        Instantiate(_enemyShipPrefab, _transform);
    }

    void SetSpawnTime()
    {
        _spawnTime = Time.time + Random.Range(_addEnemyDelayMin, _addEnemyDelayMax);
    }

    void OnEnemiesUpdated(int enemies)
    {
        if (enemies < 1)
        {
            SpawnBoss();
        }
    }

    void SpawnBoss()
    {
        if (_bossSpawned) return;
        _bossSpawned = true;
        Instantiate(_bossPrefab, _transform);
    }

    void OnSectorUpdated(int sector)
    {
        _bossSpawned = false;
    }
}
