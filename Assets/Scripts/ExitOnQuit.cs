using UnityEngine;

public class ExitOnQuit : MonoBehaviour
{
    void Start()
    {
        UserInput.Instance.OnQuitPressed += QuitApplication;
    }

    void QuitApplication()
    {
        Debug.Log("QuitApplication()");
        UserInput.Instance.OnQuitPressed -= QuitApplication;
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
