using UnityEngine;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] GameObject _gameOverPanel;

    void Start()
    {
        GameManager.Instance.GameStateChanged += OnGameStateChanged;
        _gameOverPanel.SetActive(false);
    }

    void OnGameStateChanged(GameState gameState)
    {
        if (gameState != GameState.GameOver) return;
        UserInput.Instance.OnFirePressed += StartGame;
        _gameOverPanel.SetActive(true);
    }

    void StartGame()
    {
        UserInput.Instance.OnFirePressed -= StartGame;
        _gameOverPanel.SetActive(false);
        GameManager.Instance.StartGame();
    }
}
