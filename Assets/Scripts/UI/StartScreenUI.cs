using System;
using UnityEngine;

public class StartScreenUI : MonoBehaviour
{
    [SerializeField] GameObject _startScreenPanel;

    void OnEnable()
    {
        UserInput.Instance.OnFirePressed += StartGame;
        _startScreenPanel.SetActive(true);
    }

    void StartGame()
    {
        UserInput.Instance.OnFirePressed -= StartGame;
        _startScreenPanel.SetActive(false);
        GameManager.Instance.StartGame();
    }
}
