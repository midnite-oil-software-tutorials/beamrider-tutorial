using TMPro;
using UnityEngine;

public class ScoreboardUI : MonoBehaviour
{
    [SerializeField] TMP_Text _score, _sector, _enemies;
    [SerializeField] Transform _torpedoesContainer;
    [SerializeField] Transform _playerLivesContainer;
    [SerializeField] GameObject _torpedoUIPrefab, _playerLifeUIPrefab;

    void Start()
    {
        GameManager.Instance.ScoreChanged += OnScoreChanged;
        GameManager.Instance.TorpedoesUpdated += OnTorpedoesUpdated;
        GameManager.Instance.PlayerLivesChanged += OnPlayerLivesChanged;
        GameManager.Instance.EnemiesUpdated += OnEnemiesUpdated;
        GameManager.Instance.SectorUpdated += OnSectorUpdated;
    }

    void OnDestroy()
    {
        GameManager.Instance.ScoreChanged -= OnScoreChanged;
        GameManager.Instance.TorpedoesUpdated -= OnTorpedoesUpdated;
        GameManager.Instance.PlayerLivesChanged -= OnPlayerLivesChanged;
        GameManager.Instance.EnemiesUpdated -= OnEnemiesUpdated;
        GameManager.Instance.SectorUpdated -= OnSectorUpdated;
    }

    void OnEnable()
    {
        OnScoreChanged(GameManager.Instance.Score);
        OnPlayerLivesChanged(GameManager.Instance.PlayerLives);
        OnTorpedoesUpdated(GameManager.Instance.Torpedoes);
        OnEnemiesUpdated(GameManager.Instance.EnemiesUntilBoss);
        OnSectorUpdated(GameManager.Instance.Sector);
    }

    void OnScoreChanged(int score)
    {
        _score.text = $"{score:D6}";
    }

    void OnTorpedoesUpdated(int torpedoes)
    {
        if (torpedoes >= 0)
        {
            UpdateChildUIElements(_torpedoesContainer, torpedoes, _torpedoUIPrefab);
        }
    }

    void OnPlayerLivesChanged(int playerLives)
    {
        if (playerLives >= 0)
        {
            UpdateChildUIElements(_playerLivesContainer, playerLives, _playerLifeUIPrefab);
        }
    }

    void OnEnemiesUpdated(int enemies)
    {
        _enemies.text = enemies.ToString();
    }

    void OnSectorUpdated(int sector)
    {
        _sector.text = $"Sector {sector:D2}";
    }

    void UpdateChildUIElements(Transform container, int elements, GameObject prefab)
    {
        int children = container.childCount;
        while (children > elements)
        {
            var child = container.GetChild(--children);
            child.SetParent(null);
            Destroy(child.gameObject);
        }

        while (children++ < elements)
        {
            Instantiate(prefab, container);
        }
    }
}
