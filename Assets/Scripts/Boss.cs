using System;
using UnityEngine;

public class Boss : MonoBehaviour, IHitBarrier
{
    [SerializeField] float _moveSpeed = 3f;
    
    Transform _transform;

    void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        _transform.position += Vector3.left * (_moveSpeed * Time.deltaTime);
    }

    public void HitBarrier()
    {
        GameManager.Instance.NextSector();
        Destroy(gameObject);
    }
}
