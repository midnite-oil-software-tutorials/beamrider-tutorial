using UnityEngine;

public class Projectile : MonoBehaviour, IHitBarrier
{
    [SerializeField] float _movementSpeed = 10f;
    
    Transform _transform;

    void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        _transform.position += Vector3.up * (_movementSpeed * Time.deltaTime);
    }

    public void HitBarrier()
    {
        Destroy(gameObject);
    }
}